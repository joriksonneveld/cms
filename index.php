<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <!-- outside code -->
  <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
  <!-- end outside code -->

  <script src='index.js'></script>
  <script src="./admin/js/language/language.js"></script>
</head>

<body>
  <!-- header -->
    <?php include('./header.html') ?>
  <!-- end header -->

  <section id="spa">

    <section>
      <a languageValue="1"></a>
      <input changeLanguage="1" id="input"></input>
    </section>
    
    <section>
      <a languageValue="2"></a>
      <input changeLanguage="2" id="input"></input>
    </section>
    
  </section>

  <!-- add btn -->
  <div>
    <button btn="addLanguage">
      +
    </button>
  </div>
  <!-- end add btn -->

  <!-- footer -->
  <?php include('./footer.html') ?>
  <!-- end footer -->
</body>

</html>