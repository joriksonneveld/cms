$(function () {
  // Temp array
  var array = [];
  array['1'] = '1';
  array['2'] = '2';

  // Refresh all languages at the loading of the page
  RefreshLanguage(array);

  // Event listener for inputfield for changing the value of the language tag
  $('[changeLanguage]').on('input', function (e) {
    
    // TODO fix text not changing when added modular
    
    // Create the tag that has the same value: [value='2']
    var tag = '[languageValue=\'' + $(this).attr('changeLanguage') + '\']';

    // Change the value of the tag
    $(tag).html(
      $(this).val()
    );

  });

  // Event listener for the add language button
  $('[btn="addLanguage"]').on('click', function(){

    // Add a new row
    $.get(ADMIN + '/html/language/languageBlock.html', function(e){
      // Get the data form the html file and append it in the spa block
      // TODO change spa block to language block
      $('#spa').append(e);

      // Change the id for the inputfields and save button
      $('[saveID="tbd"]').each(function(){

        // You can use the length var because this is a promice
        $(this).attr('saveID', length + '');
      });

    });

    // Add a new var in the array
    var length = array.length;
    array[length] = array.length + '';


    // Save to db
    // TODO
  });

  // TODO Move, not called cause added modular
  $('[saveID="3"]').click(function(){
    console.log('dawd');
  });

});

// Refresh all the language tags in the screen
function RefreshLanguage(array){
  var languageTags = $('[languageValue]');

  // Loop throught all the texts and change the values based on the text array
  languageTags.each(function () {
    $(this).html(array[
      $(this).attr('languageValue')
    ]);
  });
}
